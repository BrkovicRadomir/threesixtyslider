# README #

This is simple jQuery plugin for 360 degree images slider. 

Included functionality are:

* autoplay
* next and previous buttons
* Scroll bar 
* Images scrolling by mouse movement

# LICENSE #  

MIT license